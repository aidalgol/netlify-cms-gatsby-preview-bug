import React from 'react';
import Img from 'gatsby-image';

const Gallery = ({ images }) => {
  return (
    <div className="columns is-multiline">
      {images.map(image => (
        <div className="column is-half is-one-third-widescreen">
          <Img
            fluid={image.fluid}
            alt="gallery photo"
          />
        </div>
      ))}
    </div>
  )
}

export default Gallery;
