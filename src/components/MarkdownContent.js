import React from 'react'
import PropTypes from 'prop-types'
import remark from 'remark';
import remarkHTML from 'remark-html';

const MarkdownContent = ({ content, className }) => (
  <div className={className} dangerouslySetInnerHTML={{
    __html: remark().use(remarkHTML).processSync(content)
  }} />
)

MarkdownContent.propTypes = {
  content: PropTypes.string,
  className: PropTypes.string,
}

export default MarkdownContent
