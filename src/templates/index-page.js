import React from 'react'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'

import Layout from '../components/Layout'
import MarkdownContent from '../components/MarkdownContent'
import Gallery from '../components/Gallery'
//import BlogRoll from '../components/BlogRoll'

const headerColor = "rgb(250, 250, 250)"

export const IndexPageTemplate = ({
  image,
  title,
  heading,
  subheading,
  welcome,
  blurb,
  rates,
  lorem,
  nonsense,
  photos
}) => (
  <div>
    <div
      className="full-width-image margin-top-0"
      style={{
        backgroundImage: `url(${
          !!image.childImageSharp ? image.childImageSharp.fluid.src : image
        })`,
        backgroundPosition: `cover`,
        backgroundAttachment: `fixed`,
      }}>
      <section className="hero"
               style={{backgroundColor: "rgba(255, 255, 255, 0.2)",
                       width: "100%",
                       textAlign: "center"}}>
        <div className="hero-body">
          <div className="container">
            <h1 className="title" style={{color: headerColor}}>{title}</h1>
            <h3 className="subtitle" style={{color: headerColor}}>{subheading}</h3>
          </div>
        </div>
      </section>
    </div>
    <section className="section">
      <div className="container">
        <h1 className="title">{welcome}</h1>
        <MarkdownContent content={blurb}>
        </MarkdownContent>
      </div>
    </section>
    <section className="section">
      <div className="tile is-ancestor">
        <div className="tile is-parent">
          <div className="tile is-child box">
            <h1 className="title">Rates</h1>
            <ul>
              {rates.map(item => (<li>${item.amount}: {item.description}</li>))}
            </ul>
          </div>
          <div className="tile is-child box">
            <h1 className="title">Lorem</h1>
            <MarkdownContent content={lorem} />
          </div>
          <div className="tile is-child box">
            <h1 className="title">Nonsense</h1>
            <MarkdownContent content={nonsense} />
          </div>
        </div>
      </div>
      <div className="tile is-ancestor">
        <div className="tile is-parent">
          <div className="tile is-child box">
            <Gallery images={photos.map(photo => (photo.childImageSharp))} />
          </div>
        </div>
      </div>
        {/* Re-enable when there are real blog posts.
        <div className="column is-12">
          <h3 className="has-text-weight-semibold is-size-2">
            Latest blog posts
          </h3>
          <BlogRoll />
          <div className="column is-12 has-text-centered">
            <Link className="btn" to="/blog">
              Read more
            </Link>
          </div>
        </div>
        */}
    </section>
  </div>
)

IndexPageTemplate.propTypes = {
  image: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  title: PropTypes.string,
  heading: PropTypes.string,
  subheading: PropTypes.string,
  welcome: PropTypes.string,
  blurb: PropTypes.string,
  lorem: PropTypes.string,
  rates: PropTypes.arrayOf(PropTypes.object),
  nonsense: PropTypes.string,
  photos: PropTypes.arrayOf(PropTypes.object),
}

const IndexPage = ({ data }) => {
  const { frontmatter } = data.markdownRemark

  return (
    <Layout>
      <IndexPageTemplate
        image={frontmatter.image}
        title={frontmatter.title}
        heading={frontmatter.heading}
        subheading={frontmatter.subheading}
        welcome={frontmatter.welcome}
        blurb={frontmatter.blurb}
        rates={frontmatter.rates}
        lorem={frontmatter.lorem}
        nonsense={frontmatter.nonsense}
        photos={frontmatter.photos}
      />
    </Layout>
  )
}

IndexPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.object,
    }),
  }),
}

export default IndexPage

export const pageQuery = graphql`
  query IndexPageTemplate {
    markdownRemark(frontmatter: { templateKey: { eq: "index-page" } }) {
      frontmatter {
        title
        image {
          childImageSharp {
            fluid(maxWidth: 2048, quality: 100) {
              ...GatsbyImageSharpFluid
            }
          }
        }
        heading
        subheading
        welcome
        blurb
        lorem
        nonsense
        photos {
          childImageSharp {
            fluid(maxWidth: 2048, quality: 100) {
              ...GatsbyImageSharpFluid
            }
          }
        }
        rates {
          amount
          description
        }
      }
    }
  }
`
